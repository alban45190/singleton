package fr.albanw.singleton;

public class Maison {

    private static Maison instance = null;
    private final int nombreDePiece = 5;
    private final int nombreDeRadiateur = 8;

    private Maison() {
        // Toujours privé pour empêcher l'instanciation
    }

    public static synchronized Maison getInstance() {
        if (instance == null) {
            instance = new Maison();
        }
        return instance;
    }

    public int getNombreDePiece() {
        return nombreDePiece;
    }

    public int getNombreDeRadiateur() {
        return nombreDeRadiateur;
    }
}
