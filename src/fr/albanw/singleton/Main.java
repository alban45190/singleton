package fr.albanw.singleton;

public class Main {

    public static void main(String[] args) {
        System.out.println("Donnée de ma maison:");
        System.out.println("Nombre de pièce: " + Maison.getInstance().getNombreDePiece());
        System.out.println("Nombre de radiateur: " + Maison.getInstance().getNombreDeRadiateur());
    }

}