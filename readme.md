# Design Pattern

## 1. Singleton
Le design pattern singleton permet de créer une classe qui ne peut être instanciée qu'une seule fois. Cela permet de créer une classe qui ne peut être instanciée qu'une seule fois et de récupérer l'instance de cette classe.